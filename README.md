# Guli Mall

An E-Commerce Platform

## Versions

`Spring Boot 2.3.2.RELEASE`

`Spring Cloud Hoxton.SR9`

`Spring Cloud Alibaba 2.2.6.RELEASE`

`Nacos 1.4.2`

[Dependencies & Versions](https://github.com/alibaba/spring-cloud-alibaba/wiki/%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E)

## Nacos Discovery

### References

[Document](https://spring-cloud-alibaba-group.github.io/github-pages/hoxton/en-us/index.html#_spring_cloud_alibaba_nacos_discovery)

[nacos启动报错:java.io.IOException: java.lang.IllegalArgumentException: db.num is null解决方法](https://blog.csdn.net/A564492203/article/details/107815933)

[nacos1.3.2解决启动报错 Unable to start embedded Tomcat](https://blog.csdn.net/weixin_42982636/article/details/108221299)

### Steps

+ Create a database named `nacos`, and run the `nacos-server-1.4.2\nacos\conf\nacos-mysql.sql` to create tables

![img](./images/nacos-tables.png)

+ Edit `nacos-server-1.4.2\nacos\conf\application.properties` to specify my own DB info

![img](./images/nacos-application-properties.jpg)

+ Edit `nacos-server-1.4.2\nacos\bin\startup.cmd` to specify my own DB info

![img](./images/nacos-startbat-cmd.jpg)

+ The configuration in `application.properties`

`spring.cloud.nacos.discovery.server-addr=127.0.0.1:8848`
`spring.application.name=<service_name>`

+ The configuration of the startup class

`@EnableDiscoveryClient`

+ Visit 127.0.0.1:8848/nacos

![img](./images/nacos-homepage.jpg)

## OpenFeign

For example, member service wants to call a method of coupon service:

```java
@RestController
@RequestMapping("coupon/coupon")
public class CouponController {

    @RequestMapping("/member/list")
    public R memberCoupons() {
        CouponEntity couponEntity = new CouponEntity();
        couponEntity.setCouponName("$10 Discount for Every $100");
        return R.ok().put("coupons", Arrays.asList(couponEntity));
    }
}
```
### Steps

+ Create a package named as `feign` which groups all the remote interfaces.

+ Create an interface `CouponFeignService`

```java
// Notice that the service name should be exactly the same as 
// which registered in application.properties
@FeignClient("gulimall-coupon")
public interface CouponFeignService {

    @RequestMapping("/coupon/coupon/member/list")
    public R memberCoupons();
}
```
+ `@EnableFeignClients(basePackages = "com.atguigu.gulimall.member.feign")` needs to be applied on 
the startup class, in this case, it's `GulimallMemberApplication`

+ During its startup, the interfaces annotated with `@FeignClient` of `feign` package will be scanned automatically

+ To invoke this interface:

  ```java
  @RestController
  @RequestMapping("member/member")
  public class MemberController {
  
      @Autowired
      CouponFeignService couponFeignService;
  
      @RequestMapping("/coupons")
      public R test() {
          MemberEntity memberEntity = new MemberEntity();
          memberEntity.setNickname("Johnny");
  
          R memberCoupons = couponFeignService.memberCoupons();
          return R.ok().put("member", memberEntity).put("coupons", memberCoupons.get("coupons"));
      }
  }
  ```

  And visit http://localhost:8000/member/member/coupons



## Nacos Config

[Document](https://spring-cloud-alibaba-group.github.io/github-pages/hoxton/en-us/index.html#_spring_cloud_alibaba_nacos_config)

+ Configure the address of the `Nacos` server in `bootstrap.properties`

```properties
# DataId By default, the `spring.application.name` configuration is combined with the file extension (the configuration format uses properties by default), and the GROUP is not configured to use DEFAULT_GROUP by default. Therefore, the Nacos Config configuration corresponding to the configuration file has a DataId of nacos-config.properties and a GROUP of DEFAULT_GROUP
spring.application.name=nacos-config
spring.cloud.nacos.config.server-addr=127.0.0.1:8848
```

+ Create configurations in *Nacos ConfigManagement*, by default, `Data ID` is the `<service-name>.properties`

![img](./images/nacos-create-configuration-1.png)

![img](./images/nacos-create-configuration-2.png)

+ To retrieve the configured values dynamically, `@RefreshScope` annotation is required

  ```java
  @RefreshScope
  @RestController
  @RequestMapping("coupon/coupon")
  public class CouponController {
      @Autowired
      private CouponService couponService;
  
      // Values of name and age can be retrieved from Nacos ConfigManagement
      
      @Value("${coupon.user.name}")
      private String name;
  
      @Value("${coupon.user.age}")
      private String age;
  
      @RequestMapping("/test")
      public R test() {
          return R.ok().put("name", name).put("age", age);
      }
  }
  ```

+ Edit the configuration registered in *Nacos ConfigManagement* and publish

![img](./images/nacos-edit-configuration.png)

  

  

============================================================================================

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/seashine/gulimall.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/seashine/gulimall/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
