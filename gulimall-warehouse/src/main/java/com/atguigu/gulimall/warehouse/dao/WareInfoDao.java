package com.atguigu.gulimall.warehouse.dao;

import com.atguigu.gulimall.warehouse.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author ${author}
 * @email ${email}
 * @date 2022-02-21 11:13:16
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
